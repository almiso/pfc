<?php
/**
 * Created by PhpStorm.
 * User: almis
 * Date: 15/03/2019
 * Time: 22:55
 */

ob_start();

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\UploadedFile;

require(__DIR__.'./../vendor/autoload.php');

require_once(__DIR__.'./../Lib/Controller/UserController.php');
require_once(__DIR__.'./../Lib/Controller/PropertyAdController.php');

session_start();

if(session_status() == PHP_SESSION_ACTIVE){
    $inactivo = 25000;

    if(isset($_SESSION['tiempo']) ) {
        $vida_session = time() - $_SESSION['tiempo'];

        if($vida_session > $inactivo)
        {
            session_destroy();
        }}

    $_SESSION['tiempo'] = time();
}


$app = new \Slim\App();

$container = $app->getContainer();
$container['upload_directory'] = __DIR__ . '/../../images';


$app->get('/', function (Request $request, Response $response) {

    $response->getBody()->write("<h1>Welcome <span>to OnClickRealEstate.com</span> <h1>");
    return $response;
});

$app->get('/propertyAd/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];

    $cnt = new PropertyAdController();

    $retcnt = $cnt->getPropertyById($id);
    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;

});

$app->get('/propertyAd/user/{id}', function (Request $request, Response $response, array $args) {
    $idUser = $args['id'];

    $cnt = new PropertyAdController();

    $retcnt = $cnt->getPropertyByUser($idUser);
    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;

});


$app->get('/extraElements/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];

    $cnt = new PropertyAdController();

    $retcnt = $cnt->getExtraElements($id);

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;
});

$app->post('/propertyAd/{idUser}/{street}/{nStreet}/{locality}/{province}/{latitude}/{longitude}/{floor}/{price}/{propertyType}/{squareMeters}/{nRooms}/{nBathrooms}/{state}/{descrip}/{buildingDate}/{extraElements}/{operationType}', function (Request $request, Response $response, array $args) {

    $token = $request->getParsedBody()['token'];

    $idUser = $args['idUser'];
    $street = $args['street'];
    $nStreet = $args['nStreet'];
    $locality = $args['locality'];
    $province = $args['province'];
    $latitude = $args['latitude'];
    $longitude = $args['longitude'];
    $floor = $args['floor'];
    $price = $args['price'];
    $propertyType = $args['propertyType'];
    $squareMeters = $args['squareMeters'];
    $nRooms = $args['nRooms'];
    $nBathrooms = $args['nBathrooms'];
    $state = $args['state'];
    $descrip = $args['descrip'];
    $buildingDate = $args['buildingDate'];
    $extraElements = $args['extraElements'];
    $operationType = $args['operationType'];

    $cnt = new PropertyAdController();
    $retcnt = $cnt->createPropertyAd($token, $idUser,$street, $nStreet, $locality, $province, $latitude
        ,$longitude, $floor, $price, $propertyType, $squareMeters,
        $nRooms, $nBathrooms, $state, $descrip, $buildingDate, $extraElements, $operationType);
    $newR = $response->withHeader('Content-type', 'application/json');

    if($retcnt == 401){
        $newR = $response->withStatus(401);
        return $newR;

    } else {
        $newR->getBody()->write(json_encode($retcnt));

        return $newR;
    }
});

$app->post('/propertyAd/{id}/uploadImgs', function (Request $request, Response $response, array $args) {
    //$token = $request->getParsedBody()['token'];

    $directory = $this->get('upload_directory');
    $id = $args['id'];
    $cnt = new PropertyAdController();
    $files = $request->getUploadedFiles();

    foreach ($files as $file) {
        if ($file->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile($directory, $file);
            $retcnt = $cnt->insertImagesPropertyAd($id, $filename);
            $response->write('uploaded ' . $filename . '<br/>');
        }
    }

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;

});

function moveUploadedFile($directory, $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    var_dump($filename, PHP_EOL);


    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    var_dump($directory, PHP_EOL);


    return $filename;
}

$app->get('/propertyAd/{id}/imgs', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $directory = $this->get('upload_directory');

    $cnt = new PropertyAdController();
    $retcnt = $cnt->getImagesById($id);

    $images = array();

    $newR = $response->withHeader('Content-type', 'application/json');

    foreach ($retcnt as $filename) {
        //$image = file_get_contents($directory . DIRECTORY_SEPARATOR . $filename, FILE_USE_INCLUDE_PATH);
        array_push($images, $filename);
    }

    $newR->getBody()->write(json_encode($images));
    return $newR;
});

$app->get('/propertyAd/img/{img}', function (Request $request, Response $response, array $args) {

    $filename = $args['img'];
    $directory = $this->get('upload_directory');

    $newR = $response->withHeader('Content-type', 'image/jpg');
    $image = file_get_contents($directory . DIRECTORY_SEPARATOR . $filename, FILE_USE_INCLUDE_PATH);

    $newR->getBody()->write($image);
    return $newR;
});

$app->get('/searchProperty/province/{province}/locality/{locality}/pricemax/{pricemax}/pricemin/{pricemin}/propertytype/{propertytype}/minsm/{minsm}/maxsm/{maxsm}/nrooms/{nrooms}/nbathrooms/{nbathrooms}/extraelements/{extraelements}/operationType/{operationType}',
    function (Request $request, Response $response, array $args) {

    $params = explode('/', $args['params']);

    $province = $args['province'];
    $locality = $args['locality'];
    $priceMax = $args['pricemax'];
    $priceMin = $args['pricemin'];
    $propertyType = $args['propertytype'];
    $minSM = $args['minsm'];
    $maxSM = $args['maxsm'];
    $nRooms = $args['nrooms'];
    $nBathrooms = $args['nbathrooms'];
    $extraElements = $args['extraelements'];
    $operationType = $args['operationType'];


    $cnt = new PropertyAdController();

    $retcnt = $cnt->searchProperty($priceMax, $priceMin, $locality, $province, $propertyType, $minSM, $maxSM, $nRooms, $nBathrooms, $extraElements, $operationType);

    $newR = $response->withHeader('Content-type', 'application/json', 'charset=utf-8');
    $newR->getBody()->write(json_encode($retcnt,JSON_UNESCAPED_UNICODE));

    return $newR;
});

$app->get('/provinces', function (Request $request, Response $response, array $args) {

    mb_internal_encoding('UTF-8');
    mb_http_output('UTF-8');
    mb_http_input('UTF-8');
    mb_regex_encoding('UTF-8');

    $cnt = new PropertyAdController();
    $retcnt = $cnt->getAllProvinces();

    $newR = $response->withHeader('Content-type', 'application/json', 'charset=utf-8');
    $newR->getBody()->write(json_encode($retcnt,JSON_UNESCAPED_UNICODE));

    return $newR;
});

$app->get('/provinces/{province}', function (Request $request, Response $response, array $args) {

    mb_internal_encoding('UTF-8');
    mb_http_output('UTF-8');
    mb_http_input('UTF-8');
    mb_regex_encoding('UTF-8');

    $province = $args['province'];

    $cnt = new PropertyAdController();
    $retcnt = $cnt->getCitiesByProvince($province);

    $newR = $response->withHeader('Content-type', 'application/json', 'charset=utf-8');
    $newR->getBody()->write(json_encode($retcnt,JSON_UNESCAPED_UNICODE));

    return $newR;
});

$app->delete('/propertyAd/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $token = $request->getParsedBody()['token'];


    $cnt = new PropertyAdController();

    $retcnt = $cnt->deletePropertyAd($token, $id);

    $newR = $response->withHeader('Content-type', 'application/json');

    if($retcnt == 401){

        $newR = $response->withStatus(401);
        return $newR;

    } else {

        $newR->getBody()->write(json_encode($retcnt));
        return $newR;
    }
});

$app->post('/signup', function (Request $request, Response $response, array $args) {

    $name = $request->getParsedBody()['name'];
    $surnames = $request->getParsedBody()['surnames'];
    $password = $request->getParsedBody()['password'];
    $phone = $request->getParsedBody()['phone'];
    $email = $request->getParsedBody()['email'];

    $cnt = new UserController();
    $retcnt = $cnt->signUp($name, $surnames, $password, $phone, $email);

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;
});

$app->post('/login', function (Request $request, Response $response, array $args) {

    $email = $request->getParsedBody()['email'];
    $password = $request->getParsedBody()['password'];

    $cnt = new UserController();
    $retcnt = $cnt->login($email, $password);
    $newR = $response->withHeader('Content-type', 'application/json');


    if($retcnt == 401){
        $newR = $response->withStatus(401);
        return $newR;

    } else {

        $newR->getBody()->write(json_encode($retcnt));
        return $newR;
    }
});

$app->delete('/logout', function (Request $request, Response $response) {

    $cnt = new UserController();
    $retcnt = $cnt->logout();

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;
});

$app->delete('/user/{id}', function (Request $request, Response $response, array $args) {

    $token = $request->getParsedBody()['token'];
    $id = $args['id'];

    $cnt = new UserController();
    $retcnt = $cnt->deleteUser($token, $id);

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;
});

$app->get('/user/{id}', function (Request $request, Response $response, array $args) {
    $token = $request->getParsedBody()['token'];
    $id = $args['id'];

    $cnt = new UserController();
    $retcnt = $cnt->getUserById($token, $id);

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;
});

$app->get('/user/propertyAd/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];

    $cnt = new UserController();
    $retcnt = $cnt->getUserByProperty($id);

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($retcnt));

    return $newR;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->run();

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
ob_end_flush();