
create database onclickrealestate;

use onclickrealestate;

create table if not exists userApp (
	id int auto_increment primary key, 
    name varchar(30) not null, 
    surnames varchar(30) not null, 
    password varchar(255) not null,
	phone numeric(9),
    email varchar(255),
    unique(email) 
);

create table if not exists propertyAd (
	id int auto_increment primary key,
    idUser int not null,
    street varchar(255) not null,
    nStreet int not null,
    locality varchar(255),
    province varchar(255),
    latitude double,
    longitude double,
    floor int, 
    price int, 
    propertyType varchar(255),
    squareMeters int,
    nRooms int, 
    nBathrooms int,
    state varchar(255),
    descrip text,
    buildingDate date, 
    publicationDate date,
    operationType varchar(1),
    foreign key (idUser) references userApp(id) on DELETE cascade on UPDATE cascade
);

create table if not exists imgs (
	id int auto_increment primary key,
    idProperty int not null,
	img varchar(255),
	foreign key (idProperty) references propertyAd(id) on DELETE cascade on UPDATE cascade
);

create table if not exists ExtraElement (
	id int auto_increment primary key, 
    element varchar(255)
);

create table if not exists propertyExtraElement (
	id int auto_increment primary key,
    idProperty int not null, 
    idExtraElement int not null,
    foreign key(idProperty) references propertyAd(id) on DELETE cascade on UPDATE cascade,
    foreign key(idExtraElement) references ExtraElement(id) on DELETE cascade on UPDATE cascade
);

create table if not exists lovedProperty (
	id int auto_increment primary key, 
	idProperty int not null, 
    idUser int not null,
    foreign key(idProperty) references propertyAd(id) on DELETE cascade on UPDATE cascade,
    foreign key(idUser) references userApp(id) on DELETE cascade on UPDATE cascade
);
