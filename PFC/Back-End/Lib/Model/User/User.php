<?php
/**
 * Created by PhpStorm.
 * User: almis
 * Date: 15/03/2019
 * Time: 22:50
 */

class User
{
    /**
     * @var integer
     */
    private $_id;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var string
     */
    private $_surname;

    /**
     * @var integer
     */
    private $_phone;

    /**
     * @var string
     */
    private $_email;



    /**
     * User constructor.
     * @param $_id
     * @param $_name
     * @param $_surname
     * @param $_phone
     * @param $_email
     * @param $_token
     */
    public function __construct($_id, $_name, $_surname, $_phone, $_email)
    {
        $this->setId($_id);
        $this->setName($_name);
        $this->setSurname($_surname);
        $this->setPhone($_phone);
        $this->setEmail($_email);
    }


    /**
     * @return integer $_id
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string $_name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string $_surname
     */
    public function getSurname()
    {
        return $this->_surname;
    }

    /**
     * @param $surname
     */
    public function setSurname($surname)
    {
        $this->_surname = $surname;
    }

    /**
     * @return integer $_phone
     */
    public function getPhone()
    {
        return $this->_phone;
    }

    /**
     * @param $phone
     */
    public function setPhone($phone)
    {
        $this->_phone = $phone;
    }

    /**
     * @return string $_email
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }


    public function toArray(){
        $arr = array();
        $arr['id'] = $this->getId();
        $arr['name'] = $this->getName();
        $arr['surnames'] = $this->getSurname();
        $arr['phone'] = $this->getPhone();
        $arr['email'] = $this->getEmail();
        return $arr;
    }
}