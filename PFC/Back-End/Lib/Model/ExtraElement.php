<?php
/**
 * Created by PhpStorm.
 * User: almis
 * Date: 28/03/2019
 * Time: 0:14
 */

class ExtraElement
{

    //Attributes

    /**
     * @var integer
     */
    private $_id;

    /**
     * @var string
     */
    private $_name;

    /**
     * ExtraElement constructor.
     * @param $_id
     * @param $_name
     */
    public function __construct($_id, $_name)
    {
        $this->setId($_id);
        $this->setName($_name);
    }

    //Getters & Setters

    /**
     * @return integer $_id
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string $_name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    public function toArray() {
        $arr = array();
        $arr['id'] = $this->getId();
        $arr['element'] = $this->getName();
        return $arr;

    }



}