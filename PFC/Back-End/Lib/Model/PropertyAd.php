<?php
/**
 * Created by PhpStorm.
 * User: almis
 * Date: 15/03/2019
 * Time: 22:54
 */

class PropertyAd
{

    //attributes

    /**
     * @var integer
     */
    private $_id;

    /**
     * @var integer
     */
    private $_idUser;

    /**
     * @var string
     */
    private $_street;

    /**
     * @var integer
     */
    private $_nStreet;

    /**
     * @var string
     */
    private $_locality;

    /**
     * @var string
     */
    private $_province;

    /**
     * @var float
     */
    private $_latitude;

    /**
     * @var float
     */
    private $_longitude;

    /**
     * @var integer
     */
    private $_floor;

    /**
     * @var integer
     */
    private $_price;

    /**
     * @var string
     */
    private $_propertyType;

    /**
     * @var integer
     */
    private $_squareMeters;

    /**
     * @var integer
     */
    private $_nRooms;

    /**
     * @var
     */
    private $_nBathrooms;

    /**
     * @var
     */
    private $_state;

    /**
     * @var
     */
    private $_description;

    /**
     * @var
     */
    private $_extraElements;

    /**
     * @var
     */
    private $_energyCertificate;

    /**
     * @var
     */
    private $_buildingDate;

    /**
     * @var
     */
    private $_publicationDate;

    /**
     * @var
     */
    private $_operationType;



    /**
     * PropertyAd constructor.
     * @param $_id
     * @param $_idUser
     * @param $_street
     * @param $_nStreet
     * @param $_locality
     * @param $_province
     * @param $_latitude
     * @param $_longitude
     * @param $_floor
     * @param $_price
     * @param $_propertyType
     * @param $_squareMeters
     * @param $_nRooms
     * @param $_nBathrooms
     * @param $_state
     * @param $_description
     * @param $_extraElements
     * @param $_energyCertificate
     * @param $_buildingDate
     * @param $_publicationDate
     * @param $_operationType
     */
    public function __construct($_id, $_idUser,$_street, $_nStreet, $_locality, $_province, $_latitude, $_longitude, $_floor, $_price,
                                $_propertyType, $_squareMeters, $_nRooms, $_nBathrooms, $_state,
                                $_description, $_extraElements, $_energyCertificate, $_buildingDate, $_publicationDate, $_operationType)
    {
        $this->setId($_id);
        $this->setIdUser($_idUser);
        $this->setStreet($_street);
        $this->setNStreet($_nStreet);
        $this->setLocality($_locality);
        $this->setProvince($_province);
        $this->setLatitude($_latitude);
        $this->setLongitude($_longitude);
        $this->setFloor($_floor);
        $this->setPrice($_price);
        $this->setPropertyType($_propertyType);
        $this->setSquareMeters($_squareMeters);
        $this->setNRooms($_nRooms);
        $this->setNBathrooms($_nBathrooms);
        $this->setState($_state);
        $this->setDescription($_description);
        $this->setExtraElements($_extraElements);
        $this->setEnergyCertificate($_energyCertificate);
        $this->setBuildingDate($_buildingDate);
        $this->setPublicationDate($_publicationDate);
        $this->setOperationType($_operationType);
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->_idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser)
    {
        $this->_idUser = $idUser;
    }


    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->_street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->_street = $street;
    }

    /**
     * @return integer1
     */
    public function getNStreet()
    {
        return $this->_nStreet;
    }

    /**
     * @param integer1 $nStreet
     */
    public function setNStreet($nStreet)
    {
        $this->_nStreet = $nStreet;
    }

    /**
     * @return mixed
     */
    public function getLocality()
    {
        return $this->_locality;
    }

    /**
     * @param mixed $locality
     */
    public function setLocality($locality)
    {
        $this->_locality = $locality;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->_province;
    }

    /**
     * @param mixed $province
     */
    public function setProvince($province)
    {
        $this->_province = $province;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->_latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->_latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->_longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude($longitude)
    {
        $this->_longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getFloor()
    {
        return $this->_floor;
    }

    /**
     * @param mixed $floor
     */
    public function setFloor($floor)
    {
        $this->_floor = $floor;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->_price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->_price = $price;
    }

    /**
     * @return mixed
     */
    public function getPropertyType()
    {
        return $this->_propertyType;
    }

    /**
     * @param mixed $propertyType
     */
    public function setPropertyType($propertyType)
    {
        $this->_propertyType = $propertyType;
    }

    /**
     * @return mixed
     */
    public function getSquareMeters()
    {
        return $this->_squareMeters;
    }

    /**
     * @param mixed $squareMeters
     */
    public function setSquareMeters($squareMeters)
    {
        $this->_squareMeters = $squareMeters;
    }

    /**
     * @return mixed
     */
    public function getNRooms()
    {
        return $this->_nRooms;
    }

    /**
     * @param mixed $nRooms
     */
    public function setNRooms($nRooms)
    {
        $this->_nRooms = $nRooms;
    }

    /**
     * @return mixed
     */
    public function getNBathrooms()
    {
        return $this->_nBathrooms;
    }

    /**
     * @param mixed $nBathrooms
     */
    public function setNBathrooms($nBathrooms)
    {
        $this->_nBathrooms = $nBathrooms;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->_state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->_state = $state;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @return mixed
     */
    public function getExtraElements()
    {
        return $this->_extraElements;
    }

    /**
     * @param mixed $extraElements
     */
    public function setExtraElements($extraElements)
    {
        $this->_extraElements = $extraElements;
    }

    /**
     * @return mixed
     */
    public function getEnergyCertificate()
    {
        return $this->_energyCertificate;
    }

    /**
     * @param mixed $energyCertificate
     */
    public function setEnergyCertificate($energyCertificate)
    {
        $this->_energyCertificate = $energyCertificate;
    }

    /**
     * @return mixed
     */
    public function getBuildingDate()
    {
        return $this->_buildingDate;
    }

    /**
     * @param mixed $buildingDate
     */
    public function setBuildingDate($buildingDate)
    {
        $this->_buildingDate = $buildingDate;
    }

    /**
     * @return mixed
     */
    public function getPublicationDate()
    {
        return $this->_publicationDate;
    }

    /**
     * @param mixed $publicationDate
     */
    public function setPublicationDate($publicationDate)
    {
        $this->_publicationDate = $publicationDate;
    }

    /**
     * @return mixed
     */
    public function getOperationType()
    {
        return $this->_operationType;
    }

    /**
     * @param mixed $operationType
     */
    public function setOperationType($operationType)
    {
        $this->_operationType = $operationType;
    }

    public function toArray() {
        $arr = array();
        $arr['id'] = $this->getId();
        $arr['idUser'] = $this->getIdUser();
        $arr['street'] = $this->getStreet();
        $arr['nStreet'] = $this->getNStreet();
        $arr['locality'] = $this->getLocality();
        $arr['province'] = $this->getProvince();
        $arr['latitude'] = $this->getLatitude();
        $arr['longitude'] = $this->getLongitude();
        $arr['floor'] = $this->getFloor();
        $arr['price'] = $this->getPrice();
        $arr['propertyType'] = $this->getPropertyType();
        $arr['squareMeters'] = $this->getSquareMeters();
        $arr['nRooms'] = $this->getNRooms();
        $arr['nBathrooms'] = $this->getNBathrooms();
        $arr['state'] = $this->getState();
        $arr['description'] = $this->getDescription();
        $arr['extraElements'] = $this->getExtraElements();
        $arr['energyCertificate'] = $this->getEnergyCertificate();
        $arr['buildingDate'] = $this->getBuildingDate();
        $arr['publicationDate'] = $this->getPublicationDate();
        $arr['operationType'] = $this->getOperationType();
        return $arr;
    }

}