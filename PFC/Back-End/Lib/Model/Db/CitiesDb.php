<?php


class CitiesDb
{
    private $conn;

    public function getAllProvinces()
    {
        mb_internal_encoding('UTF-8');
        mb_http_output('UTF-8');
        mb_http_input('UTF-8');
        mb_regex_encoding('UTF-8');
        $this->openConnection();

        mysqli_set_charset('utf8');

        $sql = "SELECT DISTINCT Provincia FROM cities";
        $stm = $this->conn->prepare($sql);

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()) {
            array_push($ret, $r['Provincia']);
        }

        return $ret;
    }

    public function getCitiesByProvince($province) {
        mb_internal_encoding('UTF-8');
        mb_http_output('UTF-8');
        mb_http_input('UTF-8');
        mb_regex_encoding('UTF-8');

        $this->openConnection();

        mysqli_set_charset('utf8');

        $sql = "SELECT Municipio FROM cities WHERE Provincia = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("s", $p);
        $p = $province;

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()) {
            array_push($ret, $r['Municipio']);
        }

        return $ret;
    }


    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }
}
