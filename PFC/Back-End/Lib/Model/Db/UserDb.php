<?php
/**
 * User: almis
 * Date: 15/03/2019
 */

require_once(__DIR__.'./../../inc/Constants.php');
require_once (__DIR__.'/../User/User.php');
session_start();

class UserDb
{

    private $conn;

    /**
     * @param $n
     * @param $s
     * @param $pd
     * @param $ph
     * @param $e
     * @return bool
     */
    public function  signUp($n, $s, $pd, $ph, $e)  {

            $this->openConnection();

            $sql = "INSERT INTO userApp (name, surnames, password, phone, email) VALUES(?,?,?,?,?)";
            $stm = $this->conn->prepare($sql);

            $stm->bind_param("sssis", $name, $surnames, $password, $phone, $email);
            $name = $n;
            $surnames = $s;
            $password = $pd;
            $phone = $ph;
            $email = $e;


            if(!$stm->execute()) {
                return $stm->errno;
            }else {
                return true;
            }
    }

    /**
     * @param $id
     * @return array
     */
    public function getUserById($id) {
        $this->openConnection();

        $sql = "SELECT * FROM userApp WHERE id = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $i);
        $i = $id;

        $stm->execute();

        $result = $stm->get_result();

        $r = $result->fetch_assoc();
        $user = new User($r['id'], $r['name'],
            $r['surnames'], $r['phone'], $r['email']);

        return $user->toArray();
    }


    /**
     * @param $email
     * @param $pwd
     * @return array|int
     */
    public function login($email, $pwd){
        $this->openConnection();

        $sql = "SELECT * FROM userApp WHERE email =  ? AND password = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("ss", $em, $upwd);
        $em = $email;
        $upwd = $pwd;

        $stm->execute();
        $result = $stm->get_result();

        $r = $result->fetch_assoc();

        if($r != false){
            $p = new OAuthProvider();
            $token = $p->generateToken(20);
            $tokenString = bin2hex($token);
            $_SESSION['token'] = $tokenString;
            $arr = array();
            $arr['id'] = $r['id'];
            $arr['token'] = $tokenString;
            return $arr;
        }
        return 401;

    }

    /**
     * @return mixed
     */
    public function logout()
    {
        session_destroy();
        return true;
    }

    /**
     * @param $token
     * @return bool
     */
    public function checkToken($token) {
        if ($token == $_SESSION['token']){
            return true;
        }else {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteUser($id) {
        $this->openConnection();

        $sql = "DELETE FROM userApp WHERE id =  ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $i);
        $i = $id;

        $stm->execute();

        return true;
    }

    /**
     * @param $id
     * @return array
     */
    public function getUserByProperty($id) {
        $this->openConnection();

        $sql = "SELECT u.id, u.name, u.surnames, u.phone, u.email  FROM userApp u, propertyAd p  WHERE u.id = p.idUser and p.id = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $i);
        $i = $id;

        $stm->execute();

        $result = $stm->get_result();

        $r = $result->fetch_assoc();
        $user = new User($r['id'], $r['name'],
            $r['surnames'], $r['phone'], $r['email']);

        return $user->toArray();
    }
    

    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }

}