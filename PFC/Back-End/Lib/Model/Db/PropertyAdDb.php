<?php
/**
 * Created by PhpStorm.
 * User: almis
 * Date: 15/03/2019
 * Time: 22:54
 */

require_once(__DIR__.'./../PropertyAd.php');
require_once(__DIR__.'./../ExtraElement.php');

class PropertyAdDb
{

    private $conn;

    /**
     * @method to get a property by its own id
     * @return mixed
     */
    public function getPropertyById($id)
    {

        $this->openConnection();

        $sql = "SELECT * FROM propertyAd WHERE id = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idP);
        $idP = $id;

        $stm->execute();
        $result = $stm->get_result();

        $r = $result->fetch_assoc();
        $propertyAd = new PropertyAd($r['id'], $r['idUser'],
            $r['street'], $r['nStreet'], $r['locality'], $r['province'],
            $r['latitude'], $r['longitude'], $r['floor'], $r['price'],
            $r['propertyType'], $r['squareMeters'], $r['nRooms'], $r['nBathrooms'],
            $r['state'], $r['descrip'],"Unknown", "None", $r['buildingDate'], $r['publicationDate'],$r['operationType']);

        $extraElements = $this->getExtraElementsOfProperty($id);
        $propertyAd->setExtraElements($extraElements);
        return $propertyAd;
    }

    /**
     * @method to get extra elements of a propertyAd
     * @param $id
     * @return array
     */
    public function getExtraElementsOfProperty($id) {
        $this->openConnection();

        $sql = "SELECT e.id, e.element FROM ExtraElement e, propertyExtraElement p  
                WHERE p.idProperty = ? and p.idExtraElement = e.id";

        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idP);
        $idP = $id;

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()){
            $extraElement = new ExtraElement($r['id'], $r['element']);
            $extraElementArr = $extraElement->toArray();
            array_push($ret, $extraElementArr);

        }
        return $ret;
    }

    /**
     * @method to get user's properties
     * @param $idUser
     * @return array
     */
    public function getPropertyByUser($idUser)
    {

        $this->openConnection();

        $sql = "SELECT * FROM propertyAd WHERE idUser = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idU);
        $idU = $idUser;

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();

        while($r = $result->fetch_assoc()) {
            $propertyAd = new PropertyAd($r['id'], $r['idUser'],
                $r['street'], $r['nStreet'], $r['locality'], $r['province'],
                $r['latitude'], $r['longitude'], $r['floor'], $r['price'],
                $r['propertyType'], $r['squareMeters'], $r['nRooms'], $r['nBathrooms'],
                $r['state'], $r['descrip'], "Unknown", "None", $r['buildingDate'], $r['publicationDate'], $r['operationType']);

            $extraElements = $this->getExtraElementsOfProperty($propertyAd->getId());
            $propertyAd->setExtraElements($extraElements);
            array_push($ret, $propertyAd->toArray());
        }

        return $ret;
    }

    /**
     * @method to create a propertyAd
     * @param $idUser
     * @param $street
     * @param $nStreet
     * @param $locality
     * @param $province
     * @param $latitude
     * @param $longitude
     * @param $floor
     * @param $price
     * @param $propertyType
     * @param $squareMeters
     * @param $nRooms
     * @param $nBathrooms
     * @param $state
     * @param $descrip
     * @param $buildingDate
     * @param $extraElements
     * @return true
     */
    public function createPropertyAd($idUser,$street, $nStreet, $locality, $province, $latitude
                                     ,$longitude, $floor, $price, $propertyType, $squareMeters,
                                     $nRooms, $nBathrooms, $state, $descrip, $buildingDate, $extraElements, $operationType)
    {
        $this->openConnection();

        $sql = "INSERT INTO propertyAd (idUser, street, nStreet, locality, province,
                latitude, longitude, floor, price, propertyType, squareMeters, nRooms, 
                nBathrooms, state, descrip, buildingDate, publicationDate, operationType) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,curdate(),?)";

        $stm = $this->conn->prepare($sql);

        $stm->bind_param("isissddiisiiissss", $idUserB, $streetB, $nStreetB, $localityB, $provinceB,
            $latitudeB, $longitudeB, $floorB, $priceB, $propertyTypeB, $squareMetersB, $nRoomsB,
            $nBathroomsB, $stateB, $descripB, $buildingDateB, $operationTypeB);


        $idUserB = $idUser;
        $streetB = $street;
        $nStreetB = $nStreet;
        $localityB = $locality;
        $provinceB = $province;
        $latitudeB = $latitude;
        $longitudeB = $longitude;
        $floorB = $floor;
        $priceB = $price;
        $propertyTypeB = $propertyType ;
        $squareMetersB = $squareMeters;
        $nRoomsB = $nRooms;
        $nBathroomsB = $nBathrooms;
        $stateB = $state;
        $descripB = $descrip;
        $buildingDateB = $buildingDate;
        $operationTypeB = $operationType;

        $stm->execute();

        $idP = $stm->insert_id;
        $this->insertExtraElements($idP, $extraElements);

        return $idP;
    }

    /**
     * @method to insertExtraElements
     * @param $idPropertyAd
     * @param $extraElements
     */
    public function insertExtraElements($idPropertyAd, $extraElements) {
        /*foreach ($extraElements as $extraElement)*/
        for ($i=0;$i<strlen($extraElements);$i++){
            $this->openConnection();

            $extraElement = intval($extraElements[$i]);
            $sql = "INSERT INTO propertyExtraElement(idProperty, idExtraElement) VALUES($idPropertyAd, $extraElement)";
            $stm = $this->conn->prepare($sql);
            $stm->execute();
        }

    }

    /**
     * @method to save image's name
     * @param $idPropertyAd
     * @param $file
     * @return bool
     */
    public function insertImagesPropertyAd($idPropertyAd, $file) {
        $this->openConnection();
        $sql = "INSERT INTO imgs (idProperty, img) VALUES(?,?)";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("is", $idP, $f);

        $idP = $idPropertyAd;
        $f = $file;

        $stm->execute();

        return true;
    }

    /**
     * @method to get image's names of one property
     * @param $idPropertyAd
     * @return array
     */
    public function getImagesById($idPropertyAd) {
        $this->openConnection();
        $sql = "SELECT img FROM imgs WHERE idProperty = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idP);
        $idP = $idPropertyAd;

        $stm->execute();

        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()){
            $p = $r['img'];
                array_push($ret, $p);
        }
        return $ret;
    }


    /**
     * @method for search properties
     * @param $priceMax
     * @param $priceMin
     * @param $locality
     * @param $province
     * @param $propertyType
     * @param $minSM
     * @param $maxSM
     * @param $nRooms
     * @param $nBathrooms
     * @param $extraElements
     * @param $operationType
     * @return array
     */
    public function searchProperty( $priceMax,  $priceMin,  $locality,  $province,
                                    $propertyType,  $minSM,  $maxSM,
                                    $nRooms,  $nBathrooms,  $extraElements, $operationType) {
        $this->openConnection();

        $sql = "SELECT * FROM propertyAd WHERE province = '{$province}' ";

        if($priceMax != null && isset($priceMax) && $priceMax != "undefined"){
            $sql .= "and price  <= {$priceMax} ";
        }

        if($priceMin!= null && isset($priceMin) && $priceMin!= "undefined"){
            $sql .= "and price >= {$priceMin} ";
        }

        if($locality!= null && isset($locality) && $locality != "undefined"){
            $sql .= "and locality = '{$locality}' ";
        }

        if($propertyType!= null && isset($propertyType) && $propertyType != "undefined"){
            $sql .= "and propertyType = '{$propertyType}' ";
        }

        if($minSM!= null && isset($minSM) && $minSM != "undefined"){
            $sql .= "and squareMeters >= {$minSM} ";
        }

        if($maxSM!= null && isset($maxSM) && $maxSM != "undefined"){
            $sql .= "and squareMeters <= {$maxSM} ";
        }

        if($nRooms!= null && isset($nRooms) && $nRooms != "undefined"){
            $sql .= "and nRooms >= {$nRooms} ";
        }

        if($nBathrooms!= null && isset($nBathrooms) && $nBathrooms != "undefined"){
            $sql .= "and nBathrooms >= {$nBathrooms} ";
        }

        if($operationType!= null && isset($operationType) && $operationType != "undefined"){
            $sql .= "and operationType = '{$operationType}' ";
        }

        $stm = $this->conn->prepare($sql);
        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()){
            $p = new PropertyAd($r['id'], $r['idUser'],
                $r['street'], $r['nStreet'], $r['locality'], $r['province'],
                $r['latitude'], $r['longitude'], $r['floor'], $r['price'],
                $r['propertyType'], $r['squareMeters'], $r['nRooms'], $r['nBathrooms'],
                $r['state'], $r['descrip'],"Unknown", "None",
                $r['buildingDate'], $r['publicationDate'],$r['operationType']);
            $realExt = $this->getExtraElementsOfProperty($p->getId());

            if ($extraElements != null && isset($extraElements) && $extraElements != "undefined") {
                if ($this->checkExtraElementsExists($realExt, $extraElements)) {
                    $p->setExtraElements($realExt);
                    array_push($ret, $p->toArray());
                }
            } elseif ($extraElements == "undefined") {
                array_push($ret, $p->toArray());
            }
        }
        return $ret;
    }

    /**
     * @return mixed
     */
    public function checkExtraElementsExists($extraElementsOfProperty, $searchExtraElements)
    {
        $searchExtraElementsArr = str_split($searchExtraElements);
        $existExtraElements = array();
        for($i = 0; $i<=sizeof($searchExtraElementsArr)-1;$i++) {
            for($j = 0; $j<=sizeof($extraElementsOfProperty)-1;$j++){
                if($extraElementsOfProperty[$j]['id'] == $searchExtraElementsArr[$i]){
                    $existExtraElements[$i]= true;
                }
            }
            if(!isset($existExtraElements[$i])){$existExtraElements[$i] = false;}
        }

        if(in_array(!true, $existExtraElements)) {
            return false;
        }else {
            return true;
        }
    }


    /**
     * @method to delete extra elements of property
     * @param $id
     * @return bool
     */
    public function deleteExtraElementsById($id){

        $this->openConnection();

        $sql = "DELETE FROM propertyExtraElement WHERE idProperty = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idP);
        $idP = $id;

        $stm->execute();

        return true;
    }


    /**
     * @method to delete propertyAd
     * @param $id
     * @return mixed
     */
    public function deletePropertyAd($id) {
        $propertyAd = $this->getPropertyById($id);
        $this->openConnection();

        $sql = "DELETE FROM propertyAd WHERE id = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idP);
        $idP = $id;

        $stm->execute();

        return $propertyAd;
    }



    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }

}