<?php
/**
 * Developer: Àlex Milà
 * Date: 15/03/2019
 * Time: 22:55
 */

require_once(__DIR__.'./../Model/Db/UserDb.php');

class UserController
{

    /**
     * @param $n
     * @param $s
     * @param $p
     * @param $ph
     * @param $e
     * @return bool
     */
    public function signUp($n, $s, $p, $ph, $e) {
        $db = new UserDb();
        return $db->signUp($n, $s, $p, $ph, $e);
    }

    /**
     * @param $token
     * @param $id
     * @return array|int
     */
    public function getUserById($token, $id) {
        if ($this->checkToken($token) && session_status() == PHP_SESSION_ACTIVE) {
            $db = new UserDb();
            return $db->getUserById($id);
        } else {
            return 401;
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function getUserByProperty($id) {
        $db = new UserDb();
        return $db->getUserByProperty($id);
    }

    /**
     * @param $email
     * @param $password
     * @return array|int
     */
    public function login($email, $password) {
        $db = new UserDb();
        return $db->login($email, $password);
    }

    /**
     * @return mixed
     */
    public function logout(){
        $db = new UserDb();
        return $db->logout();
    }

    /**
     * @param $token
     * @param $id
     * @param $n
     * @param $s
     * @param $pd
     * @param $ph
     * @param $e
     * @return int
     */
    public function updateUser($token, $id, $n, $s, $pd, $ph, $e) {
        if ($this->checkToken($token) && session_status() == PHP_SESSION_ACTIVE) {
            $db = new UserDb();
            return $db->updateUser($id, $n, $s, $pd, $ph, $e);
        } else {
            return 401;
        }
    }

    /**
     * @param $token
     * @param $id
     * @return bool|int
     */
    public function deleteUser($token, $id) {
        if ($this->checkToken($token) && session_status() == PHP_SESSION_ACTIVE) {
            $db = new UserDb();
            return $db->deleteUser($id);
        } else {
            return 401;
        }

    }

    /**
     * @param $token
     * @return bool
     */
    public function checkToken($token) {
        $db = new UserDb();
        return $db->checkToken($token);
    }

}