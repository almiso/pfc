<?php
/**
 * Created by PhpStorm.
 * User: almis
 * Date: 15/03/2019
 * Time: 22:55
 */

require_once(__DIR__.'./../Model/Db/PropertyAdDb.php');
require_once(__DIR__.'./../Model/Db/CitiesDb.php');
require_once(__DIR__.'/UserController.php');

class PropertyAdController
{
    /**
     * @param $id
     * @return mixed
     */
    public function getPropertyById($id) {
            $db = new PropertyAdDb();
            $p = $db->getPropertyById($id);
            return $p->toArray();
    }

    /**
     * @param $idUser
     * @return array
     */
    public function getPropertyByUser($idUser) {
        $db = new PropertyAdDb();
        return $db->getPropertyByUser($idUser);
    }

    /**
     * @param $id
     * @return array
     */
    public function getExtraElements($id) {
        $db = new PropertyAdDb();
        $arr = $db->getExtraElementsOfProperty($id);
        return $arr;
    }

    /**
     * @param $token
     * @param $idUser
     * @param $street
     * @param $nStreet
     * @param $locality
     * @param $province
     * @param $latitude
     * @param $longitude
     * @param $floor
     * @param $price
     * @param $propertyType
     * @param $squareMeters
     * @param $nRooms
     * @param $nBathrooms
     * @param $state
     * @param $descrip
     * @param $buildingDate
     * @param $extraElements
     * @param $operationType
     * @return int|true
     */
    public function createPropertyAd($token, $idUser,$street, $nStreet, $locality, $province, $latitude
                                        ,$longitude, $floor, $price, $propertyType, $squareMeters,
                                     $nRooms, $nBathrooms, $state, $descrip, $buildingDate, $extraElements, $operationType)
    {
        if ($this->checkToken($token) && session_status() == PHP_SESSION_ACTIVE) {
            $db = new PropertyAdDb();
            return $db->createPropertyAd($idUser, $street, $nStreet, $locality, $province, $latitude
                , $longitude, $floor, $price, $propertyType, $squareMeters,
                $nRooms, $nBathrooms, $state, $descrip, $buildingDate, $extraElements, $operationType);
        }else {
            return 401;
        }
    }

    /**
     * @param $idProperty
     * @param $filename
     * @return bool
     */
    public function insertImagesPropertyAd($idProperty, $filename) {
        $db = new PropertyAdDb();
        return $db->insertImagesPropertyAd($idProperty, $filename);
    }

    /**
     * @param $idPropertyAd
     * @return array
     */
    public function getImagesById($idPropertyAd) {
        $db = new PropertyAdDb();
        return $db->getImagesById($idPropertyAd);
    }

    /**
     * @param $priceMax
     * @param $priceMin
     * @param $locality
     * @param $province
     * @param $propertyType
     * @param $minSM
     * @param $maxSM
     * @param $nRooms
     * @param $nBathrooms
     * @param $extraElements
     * @param $operationType
     * @return array
     */
    public function searchProperty($priceMax,$priceMin, $locality, $province,
                                    $propertyType, $minSM, $maxSM,
                                    $nRooms,  $nBathrooms,  $extraElements, $operationType) {
        $db = new PropertyAdDb();
        $properties = $db->searchProperty( $priceMax,  $priceMin,  $locality,  $province,
                                    $propertyType,  $minSM,  $maxSM,
                                    $nRooms,  $nBathrooms, $extraElements, $operationType);
        return $properties;
    }

    /**
     * @param $token
     * @param $id
     * @return int
     */
    public function deletePropertyAd($token, $id) {
        if ($this->checkToken($token) && session_status() == PHP_SESSION_ACTIVE) {
            $db = new PropertyAdDb();
            $p = $db->deletePropertyAd($id);
            return $p->toArray();
        } else {
            return 401;
        }
    }

    /**
     * @return array
     */
    public function getAllProvinces() {
        mb_internal_encoding('UTF-8');
        mb_http_output('UTF-8');
        mb_http_input('UTF-8');
        mb_regex_encoding('UTF-8');

        $db = new CitiesDb();
        return $db->getAllProvinces();
    }

    /**
     * @param $province
     * @return array
     */
    public function getCitiesByProvince($province) {
        mb_internal_encoding('UTF-8');
        mb_http_output('UTF-8');
        mb_http_input('UTF-8');
        mb_regex_encoding('UTF-8');

        $db = new CitiesDb();
        return $db->getCitiesByProvince($province);
    }

    /**
     * @param $token
     * @return bool
     */
    public function checkToken($token) {
        $cnt = new UserController();
        return $cnt->checkToken($token);
    }

}