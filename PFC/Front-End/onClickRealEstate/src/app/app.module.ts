import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FileUploadModule } from 'ng2-file-upload';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MainpageComponent } from './components/mainpage/mainpage.component';
import { AppRoutingModule } from './app-routing.module';
import { FooterComponent } from './components/footer/footer.component';
import { DetailPropertyComponent } from './components/detail-property/detail-property.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { SearchPropertiesComponent } from './components/search-properties/search-properties.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { HttpClientModule } from '@angular/common/http';
import { PropertyAddComponent } from './components/property-add/property-add.component';
import { UserSiteComponent } from './components/user-site/user-site.component';
import { UploadImagesComponent } from './components/upload-images/upload-images.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainpageComponent,
    FooterComponent,
    DetailPropertyComponent,
    SignUpComponent,
    SearchPropertiesComponent,
    ContactUsComponent,
    PropertyAddComponent,
    UserSiteComponent,
    UploadImagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FileUploadModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
