class UserApp {
    constructor( private id: number,
                 private name: string,
                 private surnames: string,
                 private phone: string,
                 private email: string ) {}
}
