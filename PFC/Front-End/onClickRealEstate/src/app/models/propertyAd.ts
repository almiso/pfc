class PropertyAd {
    constructor(public id: number,
                private idUser: number,
                private street: string,
                private nStreet: number,
                private locality: string,
                private province: string,
                public latitude: number,
                public longitude: number,
                private floor: number,
                private price: number,
                private propertyType: string,
                private squareMeters: number,
                private nRooms: number,
                private nBathrooms: number,
                private state: string,
                private description: string,
                public extraElements: [],
                private energyCertificate: string,
                private buildingDate: string,
                private publicationDate: string,
                private operationType: string) { }

}
