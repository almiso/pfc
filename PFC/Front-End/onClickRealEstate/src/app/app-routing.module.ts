import { UploadImagesComponent } from './components/upload-images/upload-images.component';
import { UserSiteComponent } from './components/user-site/user-site.component';
import { AuthGuard } from './guards/auth.guard';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { MainpageComponent } from './components/mainpage/mainpage.component';
import { DetailPropertyComponent } from './components/detail-property/detail-property.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchPropertiesComponent } from './components/search-properties/search-properties.component';
import { PropertyAddComponent } from './components/property-add/property-add.component';


const appRoutes: Routes = [
  // tslint:disable-next-line:max-line-length
  {path: 'search/:pricemax/:pricemin/:locality/:province/:propertyType/:minSM/:maxSM/:nRooms/:nBathrooms/:extraElements/:operationType', component: SearchPropertiesComponent},
  {path: 'search', component: SearchPropertiesComponent},
  {path: 'detail/:id', component: DetailPropertyComponent},
  {path: 'signup', component: SignUpComponent},
  {path: 'contactus', component: ContactUsComponent},
  {path: 'propertyAd', component: PropertyAddComponent, canActivate: [AuthGuard]},
  {path: 'uploadImages/:id', component: UploadImagesComponent, canActivate: [AuthGuard]},
  {path: 'userSite', component: UserSiteComponent, canActivate: [AuthGuard]},
  // {path: 'updateUser', component: SignUpComponent, canActivate: [AuthGuard]},
  {path: 'index', component: MainpageComponent},
  {path: '', component: MainpageComponent },
  {path: '**', component: MainpageComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
