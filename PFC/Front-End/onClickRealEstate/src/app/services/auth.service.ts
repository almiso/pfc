import { RequestOptions, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Body } from '@angular/http/src/body';
import { Router } from '@angular/router';

interface Response {
  id: string;
  token: string;
}

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  apiUrl = environment.apiUrl;
  public showErrorMessage;
  public showEmailNotValid;

  constructor(private http: HttpClient, private route: Router) { }

  login(mail, pass) {
    return this.http.post<Response>(this.apiUrl + '/login',
      {
        email: `${mail}`,
        password: `${pass}`})
        .subscribe(
          res => {
            if (res !== null && res !== undefined) {
              localStorage.setItem('token', res.token);
              localStorage.setItem('id', res.id);
              this.showErrorMessage = false;
            }
          }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401) {
                this.setError();
              }
            }
          });
  }

  isLoggedIn() {
      const token = localStorage.getItem('token');
      if (token === null || token === undefined) {
        return false;
      } else {
        return true;
      }
  }

  logout() {
    localStorage.clear();
    this.http.delete(this.apiUrl + 'logout');
  }


  setError() {
    this.showErrorMessage = true;
  }

  createUser(name, surnames, email, password, phone) {
    return this.http.post(this.apiUrl + '/signup',
    {
      'name': `${name}`,
      'surnames': `${surnames}`,
      'password': `${password}`,
      'phone': `${phone}`,
      'email': `${email}`
    })
      .subscribe( res => {
          if (res == 1062) {
            this.showEmailNotValid = true;
          }
      });
  }

  updateUser(token, id, name, surnames, email, password, phone) {
    console.log(token);
    console.log(id);
    console.log(name);
    console.log(surnames);
    console.log(email);
    console.log(password);
    console.log(phone);

    let bodyObj = {
      "token": `${token}`,
      "name": `${name}`,
      "surnames": `${surnames}`,
      "password": `${password}`,
      "phone": `${phone}`,
      "email": `${email}`
    };
    return this.http.put(this.apiUrl + `/user/${id}`, JSON.stringify(bodyObj), { headers: { 'Content-Type': 'application/json' } }).subscribe(
      data => {},
      err => { console.log(err);}
    );
  }

  getUser(token) {
    const i = localStorage.getItem('id');
    return this.http.get<UserApp>(this.apiUrl + `/user/${i}`, token);
  }

  deleteUser(token, id) {
    this.http.delete(this.apiUrl + `/user/${id}`, token ).subscribe(data =>{
      this.logout();
      this.route.navigate(['/index']);
      window.location.reload();
    });
  }

  getUserByProperty(id) {
    return this.http.get<UserApp>(this.apiUrl + `/user/propertyAd/${id}`);
  }

}
