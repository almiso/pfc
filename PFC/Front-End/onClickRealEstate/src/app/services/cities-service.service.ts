import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CitiesServiceService {

  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAllProvinces() {
   return this.http.get(this.apiUrl + '/provinces');
  }

  getCitiesByProvince(province) {
    return this.http.get(this.apiUrl + `/provinces/${province}`);
   }

searchProperties( priceMax, priceMin, locality, province, propertyType, minSM, maxSM, nRooms, nBathrooms, extraElements,
                  operationType ) {

                            let route = this.apiUrl + '/searchProperty';

                            if ( province !== null) {
                              route = route + `/province/${province}`;
                            } else {
                              route = route + '/province/undefined';
                            }

                            if ( locality !== null) {
                              route = route + `/locality/${locality}`;
                            } else {
                              route = route + '/locality/undefined';
                            }

                            if ( priceMax !== null) {
                              route = route + `/pricemax/${priceMax}`;
                            } else {
                              route = route + '/pricemax/undefined';
                            }

                            if ( priceMin !== null) {
                              route = route + `/pricemin/${priceMin}`;
                            } else {
                              route = route + '/pricemin/undefined';
                            }

                            if ( propertyType !== null) {
                              route = route + `/propertytype/${propertyType}`;
                            } else {
                              route = route + '/propertytype/undefined';
                            }

                            if ( minSM !== null) {
                              route = route + `/minsm/${minSM}`;
                            } else {
                              route = route + '/minsm/undefined';
                            }

                            if ( maxSM !== null) {
                              route = route + `/maxsm/${maxSM}`;
                            } else {
                              route = route + '/maxsm/undefined';
                            }

                            if ( nRooms !== null) {
                              route = route + `/nrooms/${nRooms}`;
                            } else {
                              route = route + '/nrooma/undefined';
                            }

                            if ( nBathrooms !== null) {
                              route = route + `/nbathrooms/${nBathrooms}`;
                            } else {
                              route = route + '/nbathrooms/undefined';
                            }

                            if ( extraElements !== null) {
                              route = route + `/extraelements/${extraElements}`;
                            } else {
                              route = route + '/extraelements/undefined';
                            }

                            if ( operationType !== null) {
                              route = route + `/operationType/${operationType}`;
                            } else {
                              route = route + '/operationType/undefined';
                            }

                            return this.http.get<PropertyAd[]>(route);
   }
}
