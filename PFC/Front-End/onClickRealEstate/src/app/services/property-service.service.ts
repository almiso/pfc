import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PropertyServiceService {

  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getPropertyById(id: number) {
    return this.http.get<PropertyAd>(this.apiUrl + `/propertyAd/${id}`);
  }

  getPropertyByUser(id: number) {
    return this.http.get(this.apiUrl + `/propertyAd/user/${id}`);
  }

  getImages(id: number) {
      return this.http.get(this.apiUrl + `/propertyAd/${id}/imgs`);
  }

  getExtraElements(id: number) {
    return this.http.get<PropertyAd>(this.apiUrl + `/propertyAd/${id}`);
  }

  deletePropertyAd(id:number, token) {
    return this.http.delete(this.apiUrl + `/propertyAd/${id}`, token ).subscribe();
  }

  createProperty(tokenp, idp, streetp, nStreetp, localityp, provincep, latitudep, longitudep, floorp, pricep, propertyTypep, squareMetersp
                ,nRoomsp, nBathroomsp, statep, descripp, buildingDatep, extraElementsp, operationTypep) {

// tslint:disable-next-line: max-line-length
                  return this.http.post<PropertyAd>(this.apiUrl + `/propertyAd/${idp}/${streetp}/${nStreetp}/${localityp}/${provincep}/${latitudep}/${longitudep}/${floorp}/${pricep}/${propertyTypep}/${squareMetersp}/${nRoomsp}/${nBathroomsp}/${statep}/${descripp}/${buildingDatep}/${extraElementsp}/${operationTypep}`, tokenp);
  }
}
