import { Component, OnInit } from '@angular/core';
import { CitiesServiceService } from 'src/app/services/cities-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
})
export class MainpageComponent implements OnInit {

  public provincies;
  public cities;
  public selectedCompra;
  public selectedProvince;
  public selectedCity;
  public selectedNrooms;
  public selectedNbathrooms;
  public selectedPrice;
  public search: PropertyAd[] = Array();

  constructor(private service: CitiesServiceService, private router: Router) { }

  ngOnInit() {
    this.service.getAllProvinces().subscribe(x => {
      this.provincies = x;
      this.provincies = this.provincies.sort();
    });
  }

  capturarCompra(newValue) {
    this.selectedCompra = newValue;
  }

  capturarProvince(newValue) {
    this.selectedProvince = newValue;
  }

  capturarCity(newValue) {
    this.selectedCity = newValue;
  }

  capturarNrooms(newValue) {
    this.selectedNrooms = newValue;
  }

  capturarNBathrooms(newValue) {
    this.selectedNbathrooms = newValue;
  }

  capturarPrice(newValue) {
    this.selectedPrice = newValue;
  }

  getCities() {
    this.service.getCitiesByProvince(this.selectedProvince).subscribe( data => {
      this.cities = data;
      this.cities = this.cities.sort();
    });
  }

  searchProperty() {
    // tslint:disable-next-line:max-line-length
    this.router.navigate([`search/${this.selectedPrice}/undefined/${this.selectedCity}/${this.selectedProvince}/undefined/undefined/undefined/${this.selectedNrooms}/${this.selectedNbathrooms}/undefined/${this.selectedCompra}` ]);
  }
}
