import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
})
export class SignUpComponent implements OnInit {

  public name;
  public surnames;
  public phone;
  public email;
  public password;
  public signup;
  public update;
  public user;

  constructor(private service: AuthService, private route: ActivatedRoute, private router: Router) {
    console.log("TCL: UserSiteComponent -> constructor -> this.route.url", );
    if ( this.route.snapshot.url[0].path === 'signup') { this.signup = true; }
    if ( this.route.snapshot.url[0].path === 'updateUser') { this.update = true; }
  }

  ngOnInit() {
    if (this.update) {
      const token = localStorage.getItem('token');
      this.service.getUser(token).subscribe( data => {
        this.user = data;
        console.log(this.user);
    });
    }
  }

  capturarName(newValue) {
    this.name = newValue;
  }

  capturarSurnames(newValue) {
    this.surnames = newValue;
  }
  capturarPhone(newValue) {
    this.phone = newValue;
  }
  capturarEmail(newValue) {
    this.email = newValue;
  }
  capturarPass(newValue) {
    this.password = newValue;
  }

  signUp() {
    const res = this.service.createUser(this.name, this.surnames, this.email, this.password, this.phone);
    this.router.navigate(['/']);
    this.service.login(this.email, this.password);
  }

  updateUser() {
    if (this.name === undefined) { this.name = this.user.name; }
    if (this.surnames === undefined) { this.surnames = this.user.surnames; }
    if (this.email === undefined) { this.email = this.user.email; }
    if (this.phone === undefined) { this.phone = this.user.phone; }
    const token = localStorage.getItem('token');
    const id = localStorage.getItem('id');

    this.service.updateUser(token, id, this.name, this.surnames, this.email, this.password, this.phone);
  }

}
