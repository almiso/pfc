import { PropertyServiceService } from 'src/app/services/property-service.service';
import { Component, OnInit } from '@angular/core';
import { CitiesServiceService } from 'src/app/services/cities-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-search-properties',
  templateUrl: './search-properties.component.html'
})
export class SearchPropertiesComponent implements OnInit {

  public operation;
  public province;
  public locality;
  public nRooms;
  public nBathrooms;
  public maxPrice;
  public minPrice;
  public propertyType;
  public minSM;
  public maxSM;
  public extraElements;
  public result: PropertyAd[] = Array();
  public images = new Array();
  public provincies;
  public cities;

  public selectedProvince = undefined;
  public selectedOperation = undefined;
  public selectedLocality = undefined;
  public selectednRooms = undefined;
  public selectednBathrooms = undefined;
  public selectedmaxPrice = undefined;
  public selectedminPrice = undefined;
  public selectedpropertyType = undefined;
  public selectedminSM = undefined;
  public selectedmaxSM = undefined;
  public selectedExtraElements = undefined;
  public parking = false;
  public ascensor = false;
  public amueblado = false;
  public calefaccion = false;
  public jardin = false;
  public piscina = false;
  public terraza = false;
  public trastero = false;

  constructor(public cityService: CitiesServiceService, public propertyService: PropertyServiceService ,
              public route: ActivatedRoute, private router: Router) {
                this.route.params.subscribe(params => {
                  this.operation = params.operationType;
                  this.province = params.province;
                  this.locality = params.locality;
                  this.nRooms = params.nRooms;
                  this.nBathrooms = params.nBathrooms;
                  this.maxPrice = params.pricemax;
                  this.minPrice = params.pricemin;
                  this.propertyType = params.propertyType;
                  this.minSM = params.minSM;
                  this.maxSM = params.maxSM;
                  this.extraElements = params.extraElements;
                });
                // tslint:disable-next-line:max-line-length
                this.cityService.searchProperties(this.maxPrice, this.minPrice, this.locality, this.province, this.propertyType, this.minSM, this.maxSM, this.nRooms, this.nBathrooms, this.extraElements, this.selectedOperation).subscribe( data => {
                  this.result = data ;
                  // tslint:disable-next-line:prefer-for-of
                  for (let index = 0; index < this.result.length; index++) {
                    // console.log(this.result[index].id);
                    this.propertyService.getImages(this.result[index].id).subscribe( imgs => {
                      this.images[`${this.result[index].id}`] = imgs[0];
                      // console.log(this.images[`${this.result[index].id}`]);
                    });
                  }
                   console.log(this.images);
                });
              }

              ngOnInit() {
                this.cityService.getAllProvinces().subscribe(x => {
                  this.provincies = x;
                  this.provincies = this.provincies.sort();
                });
              }

              capturarCompra(newValue) {
                this.selectedOperation = newValue;
              }

              capturarProvince(newValue) {
                this.selectedProvince = newValue;
              }

              capturarCity(newValue) {
                this.selectedLocality = newValue;
              }

              capturarNrooms(newValue) {
                this.selectednRooms = newValue;
              }

              capturarNBathrooms(newValue) {
                this.selectednBathrooms = newValue;
              }

              capturarmaxSM(newValue) {
                this.selectedmaxSM = newValue;
              }

              capturarminSM(newValue) {
                this.selectedminSM = newValue;
              }

              capturarMaxPrice(newValue) {
                this.selectedmaxPrice = newValue;
              }

              capturarMinPrice(newValue) {
                this.selectedminPrice = newValue;
              }

              capturarType(newValue) {
                this.selectedpropertyType = newValue;
              }

              getCities() {
                this.cityService.getCitiesByProvince(this.selectedProvince).subscribe( data => {
                  this.cities = data;
                  this.cities = this.cities.sort();
                });
              }

              searchAdvanced() {
                if (this.parking) {
                  this.selectedExtraElements = this.selectedExtraElements.concat('1');
                }
                if (this.ascensor) {
                  // tslint:disable-next-line:no-unused-expression
                  this.selectedExtraElements = this.selectedExtraElements.concat('2');
                }
                if (this.amueblado) {
                  this.selectedExtraElements = this.selectedExtraElements.concat('3');
                }
                if (this.calefaccion) {
                  this.selectedExtraElements = this.selectedExtraElements.concat('4');
                }
                if (this.jardin) {
                  this.selectedExtraElements = this.selectedExtraElements.concat('5');
                }
                if (this.piscina) {
                  this.selectedExtraElements = this.selectedExtraElements.concat('6');
                }
                if (this.terraza) {
                  this.selectedExtraElements = this.selectedExtraElements.concat('7');
                }
                if (this.trastero) {
                  this.selectedExtraElements = this.selectedExtraElements.concat('8');
                }
              }

              // public getImages(idProperty) {
              //   this.propertyService.getImages(idProperty).subscribe(response => {
              //     this.images[`idProperty`] 
              //   });
              // }

}
