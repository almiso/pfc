import { CitiesServiceService } from 'src/app/services/cities-service.service';
import { PropertyServiceService } from './../../services/property-service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { resetCompiledComponents } from '@angular/core/src/render3/jit/module';

@Component({
  selector: 'app-property-add',
  templateUrl: './property-add.component.html'
})
export class PropertyAddComponent implements OnInit {


  public provincies;
  public cities;
  public selectedProvince;
  public selectedLocality;
  public street;
  public nStreet;
  public floor;
  public sm;
  public nRooms;
  public nBathrooms;
  public operationType;
  public propertyType;
  public buildingDate;
  public state;
  public price;
  public descrip;
  public extraElements = [];
  public lat;
  public lng;
  public selectedLat;
  public selectedLng;
  public parking = false;
  public ascensor = false;
  public amueblado = false;
  public calefaccion = false;
  public jardin = false;
  public piscina = false;
  public terraza = false;
  public trastero = false;

  constructor(private cityService: CitiesServiceService, private propertyService: PropertyServiceService, private route: Router) { }

  ngOnInit() {
    this.cityService.getAllProvinces().subscribe(x => {
      this.provincies = x;
      this.provincies = this.provincies.sort();
    });
    this.getLocation();
  }

  getCities() {
    this.cityService.getCitiesByProvince(this.selectedProvince).subscribe( data => {
      this.cities = data;
      this.cities = this.cities.sort();
    });
  }

  capturarProvince(newValue) {
    this.selectedProvince = newValue;
  }

  capturarCity(newValue) {
    this.selectedLocality = newValue;
  }

  capturarStreet(newValue) {
    this.street = newValue;
  }

  capturarNStreet(newValue) {
    this.nStreet = newValue;
  }

  capturarFloor(newValue) {
    this.floor = newValue;
  }

  capturarSM(newValue) {
    this.sm = newValue;
  }

  capturarNrooms(newValue) {
    this.nRooms = newValue;
  }

  capturarNbathrooms(newValue) {
    this.nBathrooms = newValue;
  }

  capturarOp(newValue) {
    this.operationType = newValue;
  }

  capturarPropertyType(newValue) {
    this.propertyType = newValue;
  }

  capturarBuil(newValue) {
    this.buildingDate = newValue;
  }

  capturarState(newValue) {
    this.state = newValue;
  }

  capturarPrice(newValue) {
    this.price = newValue;
  }

  capturarDescrip(newValue) {
    this.descrip = newValue;
  }

  capturarLat(newValue) {
    this.selectedLat = newValue;
  }

  capturarLng(newValue) {
    this.selectedLng = newValue;
  }



  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  createProperty() {
    this.extraElements = [];
    if (this.parking) {
      this.extraElements.push(1);
    }
    if (this.ascensor) {
      this.extraElements.push(2);
    }
    if (this.amueblado) {
      this.extraElements.push(3);
    }
    if (this.calefaccion) {
      this.extraElements.push(4);
    }
    if (this.jardin) {
      this.extraElements.push(5);
    }
    if (this.piscina) {
      this.extraElements.push(6);
    }
    if (this.terraza) {
      this.extraElements.push(7);
    }
    if (this.trastero) {
      this.extraElements.push(8);
    }

    const token = localStorage.getItem('token');
    const idUser = localStorage.getItem('id');
    if(this.selectedLat === undefined) { 
      if(this.lat === undefined ) {this.selectedLat = 0; }else {
        this.selectedLat = this.lat;
      }}

    if(this.selectedLng === undefined) { 
      if(this.lng === undefined ) {this.selectedLng = 0; }else {
        this.selectedLng = this.lng;
      }}

    this.propertyService.createProperty( token, idUser, this.street, this.nStreet , this.selectedLocality, this.selectedProvince,
                                         this.selectedLat, this.selectedLng, this.floor, this.price, this.propertyType, this.sm,
                                         this.nRooms, this.nBathrooms, this.state, this.descrip, this.buildingDate, this.extraElements,
                                        this.operationType).subscribe( response => {
                                            console.log(response);
                                            this.route.navigate(['/uploadImages', response]);
                                        }, err => {
                                            console.log(err);
                                        });
  }

}
