import { Router } from '@angular/router';
import { PropertyServiceService } from './../../services/property-service.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-user-site',
  templateUrl: './user-site.component.html'
})
export class UserSiteComponent implements OnInit {

  properties;

  constructor(private service: PropertyServiceService, private userService: AuthService, private route: Router) {}

  ngOnInit() {
    const idUser = localStorage.getItem('id');
// tslint:disable-next-line: radix
    this.service.getPropertyByUser(parseInt(idUser)).subscribe(
      data => {
         this.properties = data;
      });
  }

  eliminarAdd(id) {
    const token = localStorage.getItem('token');
    console.log(id);
    this.service.deletePropertyAd(id, token);
    window.location.reload();
  }

  deleteUser() {
    const idUser = localStorage.getItem('id');
    const token = localStorage.getItem('token');

    this.userService.deleteUser(token, idUser);
  }

}
