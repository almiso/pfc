import { environment } from './../../../environments/environment';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-upload-images',
  templateUrl: './upload-images.component.html'
})
export class UploadImagesComponent implements OnInit {

  selectedFile: File = null;
  @Output() event = new EventEmitter();
  public image: any;
  public id;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
  }

  ngOnInit(): void {
  }

  /**
   *  Validate uploader params
   * @param event
   */
  public onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
    if (this.selectedFile) {
      return this.upload();
    }
  }

  upload() {
    const fd = new FormData();
    fd.append('image', this.selectedFile, this.selectedFile.name);
    this.http.post(environment.apiUrl + `/propertyAd/${this.id}/uploadImgs`, fd).subscribe((res: any) => {
      this.image = res.data;
      this.event.emit(this.image);
    }, (err: any) => {
        console.log(err);
    });
  }

  finalizar() {
    this.router.navigate(['/userSite']);
  }
}
