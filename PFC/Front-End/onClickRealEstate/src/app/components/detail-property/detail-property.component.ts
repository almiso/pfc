import { AuthService } from 'src/app/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { PropertyServiceService } from 'src/app/services/property-service.service';
import { ActivatedRoute } from '@angular/router';
declare let L;

@Component({
  selector: 'app-detail-property',
  templateUrl: './detail-property.component.html'
})
export class DetailPropertyComponent implements OnInit {

  public images: any;
  public propertyAd: PropertyAd = undefined;
  public extraElements: any[];
  private id: number;
  public contact: UserApp;

  constructor(private service: PropertyServiceService, private route: ActivatedRoute, private authService: AuthService ) {
   }

  ngOnInit() {
    this.route.params.subscribe(paramsId => {
      this.id = paramsId.id;
    });
    this.service.getPropertyById(this.id).subscribe( property => {
        this.propertyAd = property;
        this.extraElements = property.extraElements;
        const map = L.map('mapa').setView( [property.latitude , property.longitude], 15);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        const circle = L.circle([property.latitude, property.longitude], {
          color: 'blue',
          fillColor: '#66A5E8',
          fillOpacity: 0.25,
          radius: 750
      }).addTo(map);

    });
    this.service.getImages(this.id).subscribe(data => {
      this.images = data;
    });

    this.authService.getUserByProperty(this.id).subscribe(response => {
      this.contact = response;
    });
  }
}
