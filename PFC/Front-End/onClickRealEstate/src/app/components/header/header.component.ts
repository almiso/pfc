import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {


  public user;
  public password;
  public error: boolean;

  constructor(private service: AuthService, private route: Router) { }

  ngOnInit() {
  }

  capturarUser(newValue) {
    this.user = newValue;
  }

  capturarPass(newValue) {
    this.password = newValue;
  }

  login() {
    this.service.login(this.user, this.password);
    if(this.service.isLoggedIn()) {
      this.route.navigate(['/']);
    }
  }
}
